# This is a repository that demonstrates the use of asciicast on Gitea 

## My study notes

### How to install & use `acsiinema`

#### Install (macOS)
For macos, I recommend using Homebrew to install asciinema.

[![install](/static/install_asciinema.png)](https://gitea.com/sillyguodong/asciicast_test/src/branch/main/cast/install_asciinema.cast)

Obviously I have installed asciinema already😂.

#### Record
[![rec](/static/rec.png)](https://gitea.com/sillyguodong/asciicast_test/src/branch/main/cast/rec.cast)


### How to build & startup `act_runner`
#### Build

[![runner build](/static/act_runner_build.png)](https://gitea.com/sillyguodong/asciicast_test/src/branch/main/cast/act_runner_build.cast)

An executable file named `act_runner` is generated.

#### Startup

[![runner startup](/static/act_runner_build.png)](https://gitea.com/sillyguodong/asciicast_test/src/branch/main/cast/act_runner_build.cast)

Please set `address` to the ip of the current host in the `.runner` file before startup. 

**It just needs to be changed during development. If you are in production, please do not modify `.runner` file.**

Then execute `act_runner` file, You will specify the configuration file and run act_runner.




